import os
import copy
from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
from werkzeug.utils import secure_filename
from collections import defaultdict

app = Flask(__name__)

app.secret_key = 'ae91rkx50ooe200331r'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'grupp31'
app.config['MYSQL_DB'] = 'web_shop'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)

UPLOAD_FOLDER = './static/images'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'jfif'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == "POST":
        details = request.form
        data = fetch_search_products(details['search'])
    else:
        data = get_products()

    price_tuple = ()
    for x in data:
        if x['sale']:
            temp_price = x['price']
            temp_sale = x['sale']
            temp_sale = float(temp_sale)
            temp_sale = float(temp_sale/100)
            x['price']=int(temp_price * (1-temp_sale))

            price_tuple = price_tuple + (x,)
        else:
            price_tuple = price_tuple + (x,)
    data = price_tuple

    return render_template('home.html', data = data, session_numb = session.get('auth'),
                           uname = session.get('uname'), uid = session.get('uid'))


@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == "POST": # IF submit button clicked, if == true. Content extracted from textbox.
        details = request.form
        uname = details['username']
        pwd = details['password']
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM Users WHERE (uname = uname  AND pwd = pwd)")
        fetchdata = cur.fetchall()
        cur.close()

        for x in fetchdata:
            uid = x['uid']
            if (x['pwd'] == pwd and x['uname'] == uname):
                if(x['role'] == "user"):
                    session['uid'] = uid
                    session['auth'] = '1'
                    session['uname'] = uname
                    session['password'] = pwd
                    return redirect('/')
                elif(x['role'] == "admin"):
                    session['uid'] = uid
                    session['auth'] = '2'
                    session['uname'] = uname
                    session['password'] = pwd
                    return redirect('/')


    return render_template('login.html')

@app.route('/register', methods=['GET','POST'])
def register():   # TODO: Create register function, html etc.
    # TODO: Make sure all usernames are unique
    # TODO: Assign unique cart to each user
    if request.method == "POST":  # IF submit button clicked, if == true. Content extracted from textbox.
        details = request.form
        uname = details['username']
        pwd = details['password']
        pwd_check = details['password_check']
        email = details['email']
        phone = details['phone']
        try:
            if pwd != pwd_check or pwd == "" or uname == "":
                return redirect('/register')
            else:
                try:                                                                                    # TRANSACTION
                    mysql.connection.autocommit = False  # Turn off autocommit. Do a transaction

                    # --------CREATE USER----------
                    cur = mysql.connection.cursor()
                    cur.execute("INSERT INTO Users(uname, pwd, email, phone, role) "
                                "VALUES (%s, %s, %s, %s, %s)", (uname, pwd, email, phone, 'user'))

                    #---------CREATE CART----------
                    cur = mysql.connection.cursor()
                    cur.execute("INSERT INTO Carts VALUES ()")

                    # ---------GET CART_ID AND USER_ID----------
                    cur = mysql.connection.cursor()
                    cur.execute("SELECT MAX(cartid) FROM Carts")
                    cartID = cur.fetchone()
                    cur.execute("SELECT MAX(uid) FROM Users")
                    userID = cur.fetchone()

                    #---------INSERT THE ABOVE VALUES INTO CARTUSERS----------
                    cur = mysql.connection.cursor()
                    cur.execute("INSERT INTO CartUsers(cartid, uid)"
                                "VALUES(%s, %s)", (cartID['MAX(cartid)'], userID['MAX(uid)']))

                    mysql.connection.commit()
                    mysql.connection.autocommit = False
                    cur.close()

                except mysql.connection.Error as error:
                    print error
                    mysql.connection.rollback()

            return redirect('/login')
        except IntegrityError:
            print("something went wrong")
        finally:
            return render_template('register.html',session_numb = session.get('auth'), uname = session.get('uname'))

    return render_template('register.html',session_numb = session.get('auth'), uname = session.get('uname'))

#
#______________PROFILE______________________
#
#
@app.route('/profile/<uname>', methods=['GET', 'POST'])
def profile(uname):
    if((session.get('auth') == '1') or (session.get('auth') == '2')):


        if request.method == "POST":  # IF submit button clicked, if == true. Content extracted from textbox.
            details = request.form
            new_username = details['username']
            pwd = details['password']
            pwd_check = details['password_check']
            email = details['email']
            phone = details['phone']

            user_id = session.get('uid')
            """
            All if statements check whether any profile information
            has been altered or not. If that is the case, the old
            account information is replaced with the new one.
            """
            try:
                if new_username:
                    cur = mysql.connection.cursor()
                    cur.execute("UPDATE Users SET uname = %s WHERE uid=%s", (new_username, user_id))
                    mysql.connection.commit()
                    cur.close()

                    uname = new_username
                    session['uname'] = new_username
                if pwd:
                    if pwd == pwd_check:
                        cur = mysql.connection.cursor()
                        cur.execute("UPDATE Users SET pwd = %s WHERE uid = %s", (pwd, user_id))
                        mysql.connection.commit()
                        cur.close()
                        session['pwd'] = pwd
                    else:
                        #insert flash here...
                        return render_template("profile.html", uname = uname, pwd = session.get('pwd'))
                if email:
                    cur = mysql.connection.cursor()
                    cur.execute("UPDATE Users SET email = %s WHERE uid = %s ", (email, user_id))
                    mysql.connection.commit()
                    cur.close()

                if phone:
                    cur = mysql.connection.cursor()
                    cur.execute("UPDATE Users  SET phone = %s WHERE uid = %s", (phone, user_id))
                    mysql.connection.commit()
                    cur.close()
                #Update new user data
                userData = getUser()
            except IntegrityError:
                print("Something went wrong")
            finally:
                userData = getUser()
                return redirect(url_for("profile", uname=uname, pwd=userData['pwd'], email=userData['email']
                                      , phone=userData['phone']))
        userData = getUser()

        allOrders = fetchUserOrders()

        allData = []
        data = {
            'uid': "",
            'uname': "",
            'ordernr': "",
            'pid': [],
            'pname': [],
            'amount': [],
            'status': ""
        }
        i = 0
        j = 0
        while i < len(allOrders):
                data['uid'] = allOrders[i]['uid']
                data['uname'] = allOrders[i]['uname']
                data['ordernr'] = allOrders[i]['ordernr']
                data['status'] = allOrders[i]['status']

                while (allOrders[i]['ordernr'] == allOrders[j]['ordernr']) and j <= len(allOrders):
                    data['pid'].append(allOrders[j]['pid'])
                    data['pname'].append(allOrders[j]['pname'])
                    data['amount'].append(allOrders[j]['amount'])
                    j = j + 1
                    if j == len(allOrders):
                        break
                i = j
                allData.append(copy.deepcopy(data))
                data['pid'] = []
                data['pname'] = []
                data['amount'] = []
        return render_template("profile.html", pwd=userData['pwd'], email=userData['email']
                                , phone=userData['phone'],session_numb = session.get('auth'),
                               uname = session.get('uname'), uid = session.get('uid'), data=allData)
    else:
        return redirect("/")

def fetchUserOrders():
    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM `order_view` WHERE uid = %s ORDER BY `ordernr`"
    cur.execute(query_string, (session.get('uid'),))
    fetchdata = cur.fetchall()
    cur.close()
    return fetchdata


def getUser():
    userID = session.get('uid')
    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM Users WHERE uid = %s"
    cur.execute(query_string, (userID,))  #
    fetchdata = cur.fetchall()
    cur.close()
    return fetchdata[0]

@app.route('/logout', methods=['GET', 'POST'])
def logout():
    if (session['auth'] == '1'or session['auth'] == '2'):
        session['auth'] = "0"
        session['uid'] = ""
        session['uname'] = ""
        session['password'] = ""
        return redirect('/')
    else:
        return redirect('/')

#
#
#_________________CART______________________
#
#

@app.route('/cart/<uid>', methods=['GET','POST'])
def cart(uid):
    if (session.get('auth') == "1"):
        cartid = fetch_cart_id(uid)

        cart = fetch_product_cart(cartid)

        product = ()
        total_price = 0

        for x in cart:
            if(cartid == x['cartid']):
                pid = x['pid']
                new_product = get_product(pid)
                for y in new_product:
                    y['amount'] = x['amount']

                    if y['sale']:
                        temp_price = y['price']
                        temp_sale = y['sale']
                        temp_sale = float(temp_sale)
                        temp_sale = float(temp_sale / 100)
                        y['price'] = int(temp_price * (1 - temp_sale))

                    total_price = total_price + (y['price'] * x['amount'])
                    break
                product = product + new_product

    return render_template('cart.html', product=product, total_price = total_price, cartid = cartid, session_numb = session.get('auth'),
                           uname = session.get('uname'), uid = session.get('uid'))

@app.route('/cart_send/<cartid>', methods=['GET','POST'])
def send_cart(cartid):
    uid = session.get("uid")

    ordernr = create_order()

    send_products_order(ordernr)

    cartid = fetch_cart_id(uid)
    remove_cart(cartid)

    return redirect(url_for("cart", uid=uid))

@app.route('/cart_remove/<pid>', methods=['GET','POST'])
def remove_cart(pid):
    uid = session.get("uid")

    cartid = fetch_cart_id(uid)

    add_product_amount(pid, fetch_cart_product_amount(cartid, pid))

    remove_from_cart(str(pid), str(cartid))

    return redirect(url_for("cart", uid=uid))

#
#
#_________________ADMIN: MANAGE USERS_____________________
#
#
@app.route('/manage_users', methods=['GET', 'POST'])
def manage_users():
    data = getAllUsers()
    return render_template("manage_users.html", data=data, uname=session['uname'], session_numb = session['auth'])

@app.route('/manage_users/<uid>', methods=['GET', 'POST'])
def deleteUser(uid):
    removeUser(uid)
    return redirect(url_for("manage_users"))

def removeUser(uid):
    cur = mysql.connection.cursor()
    query_string = " DELETE FROM Users WHERE uid = %s"
    cur.execute(query_string, (uid,))
    mysql.connection.commit()
    cur.close()

def getAllUsers():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Users")
    fetchdata = cur.fetchall()
    cur.close()
    return fetchdata

#
#
#_________________ADMIN: MANAGE ORDERS_____________________
#
#
@app.route('/manage_orders', methods=['GET', 'POST'])
def manage_orders():
    allOrders = getAllOrders()
    allData=[]
    data = {
        'uid': "",
        'uname':"",
        'ordernr':"",
        'pid':[],
        'pname':[],
        'amount':[],
        'status':""
    }
    i=0
    j=0
    try:
        while i < len(allOrders):
            data['uid']=allOrders[i]['uid']
            data['uname'] = allOrders[i]['uname']
            data['ordernr'] = allOrders[i]['ordernr']
            data['status'] = allOrders[i]['status']

            while (allOrders[i]['ordernr']==allOrders[j]['ordernr']) and j <= len(allOrders):
                data['pid'].append(allOrders[j]['pid'])
                data['pname'].append(allOrders[j]['pname'])
                data['amount'].append(allOrders[j]['amount'])
                j=j+1
                if j == len(allOrders):
                    break
            i=j
            allData.append(copy.deepcopy(data))
            data['pid'] = []
            data['pname'] = []
            data['amount'] = []
    except IndexError:
        # ta bort try except om if satsten fungerar
        return render_template("manage_orders.html", data=allData, uname=session['uname'], session_numb=session['auth'])
    finally:
        return render_template("manage_orders.html", data=allData, uname=session['uname'], session_numb=session['auth'])


def getAllOrders():
    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM `order_view` ORDER BY `ordernr`"
    cur.execute(query_string)
    fetchdata = cur.fetchall()
    cur.close()
    return fetchdata
#
#
#_________________ADMIN: MANAGE ORDERS ACTIONS_____________________
#
#

@app.route('/send_order/<ordernr>', methods=['GET', 'POST'])
def send_order(ordernr):
    sendOrder(ordernr)
    return redirect(url_for("manage_orders"))

def sendOrder(ordernr):
    send_status = 1
    cur = mysql.connection.cursor()
    query_string = " UPDATE Orders SET status = %s WHERE ordernr = %s"
    cur.execute(query_string, (send_status, ordernr))
    mysql.connection.commit()
    cur.close()

@app.route('/cancel_order/<ordernr>', methods=['GET', 'POST'])
def cancel_order(ordernr):
    removeOrder(ordernr)
    return redirect(url_for("manage_orders"))

def removeOrder(ordernr):
    cur = mysql.connection.cursor()
    query_string = " DELETE FROM Orders WHERE ordernr = %s"
    cur.execute(query_string, (ordernr,))
    mysql.connection.commit()
    cur.close()


#
#
#_________________PRODUCT_____________________
#
#

@app.route('/product/<prodname>', methods=['GET', 'POST'])
def product(prodname):
    stock_limit_exceeded = 0
    sale_flag = 0
    #Fetches the product id that is connected to the product name
    product_id = fetch_product_id(prodname)
    #if someone presses the "Add to cart "button, will remove amount from (Products) and add amount to (CartProducts)

    if request.method == 'POST':
        user_id = session.get('uid')
        details = request.form

        if request.form['submit'] == "Add to cart":
            amount=details['amount']
            amount=int(amount)

            product_amount = fetch_product_amount(product_id)

            if product_amount >= amount:
                remove_product_amount(product_id, amount)

                cart_id = fetch_cart_id(user_id)

                add_to_cart(cart_id, product_id, amount)
                return redirect(url_for("product", prodname=prodname))
            else:
                stock_limit_exceeded = 1
        #If the rating button is pressed
        elif request.form['submit'] == "Rate!":
            select = request.form.get('rate_select')
            select=int(select)
            comment=details['comment']

            insert_to_ratings(product_id, user_id, select, comment, session.get('uname'))
            return redirect(url_for("product", prodname=prodname))

    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM Products WHERE pid = %s"  # Use this select template to fetch data from database
    rows_count = cur.execute(query_string, (product_id,))  # This one toooo
    fetchdata = cur.fetchall()
    cur.close()

    # Initiating values
    temp_image_path = ""
    temp_description = ""
    temp_amount = 0
    temp_price = 0

    # Check if there was a row with the correct product name (if product exist)
    if rows_count <= 0:
        temp_not_exist_flag = 1

    else:
        # Fetches values from the selected row
        for x in fetchdata:
            temp_image_path = x['image_path']
            temp_description = x['description']
            temp_amount = x['amount']
            temp_price = x['price']
            if x['sale'] and x['sale'] != 0:
                temp_sale = x['sale']
                input_sale = temp_sale
                temp_sale = float(temp_sale)
                temp_sale = float(temp_sale/100)
                sale_flag = 1
            else:
                input_sale = None
                temp_sale=0
        original_price = temp_price
        temp_price = float(temp_price)
        final_price = int(temp_price * (1-temp_sale))
        temp_not_exist_flag = 0

    # Gets the session number of the currently logged in user
    session_numb = session.get('auth')

    # Gets average rating of product
    rating = fetch_avg_rating(product_id)
    if rating != None:
        rating = round(rating, 2)

    # Get data for previous rating posts
    ratedata = fetch_rating_values(product_id)

    return render_template('product.html', pname=prodname, image_path=temp_image_path, description=temp_description,
                               amount=temp_amount, price=final_price, original_price=original_price,  ratedata=ratedata,
                               not_exist_flag=temp_not_exist_flag, stock_limit_exceeded=stock_limit_exceeded,
                               session_numb=session_numb, sale_flag=sale_flag,
                                sale=input_sale, rating=rating,
                               uname=session.get('uname'), uid = session.get('uid'))


#
#
#_________________MANAGE PRODUCTS_____________________
#
#


@app.route('/add_product', methods=['GET', 'POST'])
def add_product():
    session_numb = session.get('auth')
    session_uname = session.get('uname')

    if request.method == 'POST':
        details=request.form
        prodname=details['productname']
        description=details['description']
        price=details['price']
        amount=details['amount']

        if 'file' not in request.files:
            print("file fail")
        # if user does not select file, browser also
        # submit an empty part without filename
        else:
            file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        else:
            filename = None
        insert_product(prodname, description, price, filename, amount)
    return render_template('add_product.html', session_numb=session_numb,
                           uname=session_uname)


@app.route('/manage_products', methods=['GET', 'POST'])
def manage_products():
    session_numb = session.get('auth')
    session_uname = session.get('uname')
    productdata = get_products()

    if request.method =='POST':
        details=request.form

        if request.form['submit'] == "Edit product":
            select = request.form.get('product_select')
            pid = int(select)

            if details['add_amount'] and not details['remove_amount']:
                add_amount=int(details['add_amount'])
                add_product_amount(pid, add_amount)

            elif details['remove_amount'] and not details['add_amount']:
                remove_amount=int(details['remove_amount'])
                remove_product_amount(pid, remove_amount)

            if details['price']:
                price=details['price']
                update_product_price(pid, price)

            if details['sale']:
                sale=details['sale']
                update_product_sale(pid, sale)

        elif request.form['submit'] == "Remove product":
            select = request.form.get('remove_product_select')
            pid = int(select)
            remove_product(pid)
            return redirect(url_for('manage_products'))

        elif request.form['submit'] == "Add a new product":
            return redirect(url_for('add_product'))

    return render_template('manage_products.html', session_numb=session_numb,
                           uname=session_uname, productdata=productdata)

def insert_product(prodname, description, price, filename, amount):
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO Products(pname, description, amount, image_path, price) "
                "VALUES (%s, %s, %s, %s, %s)",
                (prodname, description, amount, filename, price,))
    mysql.connection.commit()
    cur.close()

def fetch_product_id(prodname):
    cur = mysql.connection.cursor()
    query_string = "SELECT pid FROM Products WHERE pname = %s"  # Use this select template to fetch data from database
    rows_count = cur.execute(query_string, (prodname,))  # This one toooo
    fetchdata = cur.fetchall()
    cur.close()
    product_id = 0
    for x in fetchdata:
        product_id = x['pid']

    return product_id

def remove_product(product_id):
    cur = mysql.connection.cursor()
    query_string = " DELETE FROM Products WHERE pid = %s"
    cur.execute(query_string, (product_id,))
    mysql.connection.commit()
    cur.close()

def get_product(product_id):
    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM Products WHERE pid = %s"  # Use this select template to fetch data from database
    cur.execute(query_string, (product_id,))  # This one toooo
    product = cur.fetchall()
    cur.close()

    return product

def get_products():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Products")
    fetchdata = cur.fetchall()
    cur.close()
    return fetchdata

def fetch_product_amount(product_id):
    cur = mysql.connection.cursor()
    query_string = "SELECT amount FROM Products WHERE pid = %s"  # Use this select template to fetch data from database
    cur.execute(query_string, (product_id,))  # This one toooo
    amountfetch = cur.fetchall()
    cur.close()
    for x in amountfetch:
        prodamount = x['amount']

    return prodamount

def remove_product_amount(product_id, amount):
    prodamount = fetch_product_amount(product_id)
    prodamount = prodamount-amount

    if prodamount <= 0:
        update_product_amount(product_id, 0)
    else:
        update_product_amount(product_id, prodamount)


def add_product_amount(product_id, amount):
    prodamount = fetch_product_amount(product_id)
    prodamount=prodamount+amount
    update_product_amount(product_id, prodamount)

def update_product_amount(product_id, prodamount):
    cur = mysql.connection.cursor()
    query_string = " UPDATE Products SET amount = %s WHERE pid = %s"
    cur.execute(query_string, (prodamount, product_id))
    mysql.connection.commit()
    cur.close()

def update_product_price(product_id, price):
    cur = mysql.connection.cursor()
    query_string = " UPDATE Products SET price = %s WHERE pid = %s"
    cur.execute(query_string, (price, product_id))
    mysql.connection.commit()
    cur.close()

def fetch_cart_id(user_id):
    cart_id = 0
    cur = mysql.connection.cursor()
    query_string = "SELECT cartid FROM CartUsers WHERE uid = %s"  # Use this select template to fetch data from database
    cur.execute(query_string, (user_id,))  # This one toooo
    amountfetch = cur.fetchall()
    cur.close()
    for x in amountfetch:
        cart_id = x['cartid']

    return cart_id

def fetch_product_cart(cart_id):
    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM CartProducts WHERE cartid = %s"
    cur.execute(query_string, (cart_id,))
    cart = cur.fetchall()
    cur.close()

    return cart

def fetch_cart_product_amount(cart_id, product_id):
    cur = mysql.connection.cursor()
    rows_count = query_string = "SELECT amount FROM CartProducts WHERE cartid = %s AND pid = %s "  # Use this select template to fetch data from database
    cur.execute(query_string, (cart_id, product_id))  # This one toooo
    amountfetch = cur.fetchall()
    cur.close()
    prodamount = 0
    if rows_count >= 0:
        for x in amountfetch:
            prodamount = x['amount']

    return prodamount

def add_to_cart(cart_id, product_id, amount):
    prodamount = fetch_cart_product_amount(cart_id, product_id)
    if prodamount == 0:
        insert_to_cart(cart_id, product_id, amount)
    else:
        prodamount = prodamount + amount
        update_cart_amount(cart_id, product_id, prodamount)

def remove_cart(cart_id):
    cur = mysql.connection.cursor()
    query_string = "DELETE FROM CartProducts WHERE cartid = %s"
    cur.execute(query_string, (cart_id,))
    mysql.connection.commit()
    cur.close()
    return

def remove_from_cart(product_id, cart_id):
    cur = mysql.connection.cursor()
    query_string = "DELETE FROM CartProducts WHERE pid = %s AND cartid = %s"
    cur.execute(query_string, (product_id, cart_id))
    mysql.connection.commit()
    cur.close()
    return

def update_cart_amount(cart_id, product_id, prodamount):
    cur = mysql.connection.cursor()
    query_string = " UPDATE CartProducts SET amount = %s WHERE cartid = %s AND pid = %s"
    cur.execute(query_string, (prodamount, cart_id, product_id))
    mysql.connection.commit()
    cur.close()
    return

def insert_to_cart(cart_id, product_id, prodamount):
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO CartProducts(cartid, pid, amount) "
                "VALUES (%s, %s, %s)", (cart_id, product_id, prodamount))
    mysql.connection.commit()
    cur.close()
    return

def insert_to_ratings(product_id, user_id, select, comment, uname):
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO Ratings(pid, uid, rating, comment, uname) "
                "VALUES (%s, %s, %s, %s, %s)", (product_id, user_id, select, comment, uname))
    mysql.connection.commit()
    cur.close()

def fetch_avg_rating(product_id):
    cur = mysql.connection.cursor()
    query_string = "SELECT AVG(rating) FROM Ratings WHERE pid = %s"  # Use this select template to fetch data from database
    cur.execute(query_string, (product_id,))  # This one toooo
    ratingfetch = cur.fetchall()
    cur.close()
    for x in ratingfetch:
        rating=x['AVG(rating)']

    return rating

def fetch_rating_values(product_id):
    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM Ratings WHERE pid = %s"  # Use this select template to fetch data from database
    cur.execute(query_string, (product_id,))  # This one toooo
    ratefetch = cur.fetchall()

    return ratefetch

def create_order():
    try:
        mysql.connection.autocommit = False # Turn off autocommit. Do a transaction

        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO Orders(status)""VALUES(0)")
        cur.close()

        cur = mysql.connection.cursor()
        cur.execute("SELECT MAX(ordernr) FROM Orders")
        ordernr = cur.fetchone()
        cur.close()

        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO OrderUsers(ordernr, uid)""VALUES (%s, %s)", (ordernr['MAX(ordernr)'], session.get('uid')))
        cur.close()

        mysql.connection.commit()

        mysql.connection.autocommit = True

    except mysql.connection.Error as error:
        print error
        mysql.connection.rollback()

    return ordernr

def send_products_order(ordernr):
    try:                                                                        #TRANSACTION
        cartid = fetch_cart_id(session.get('uid'))
        cart = fetch_product_cart(cartid)

        mysql.connection.autocommit = False  # Turn off autocommit. Do a transaction

        for x in cart:
            cur = mysql.connection.cursor()
            if (cartid == x['cartid']):
                pid = x['pid']
                amount = x['amount']
                insert_order_product(ordernr['MAX(ordernr)'], pid, amount, cur)
                cur.close()



        mysql.connection.commit()
        mysql.connection.autocommit = True

    except mysql.connection.Error as error:
        print error
        mysql.connection.rollback()

def insert_order_product(ordernr, pid, amount, cur):
    cur.execute("INSERT INTO OrderProducts(ordernr,pid,amount)""VALUES (%s, %s, %s)", (ordernr, pid, amount))

def fetch_search_products(search_string):
    input_string = "%" + search_string + "%"
    cur = mysql.connection.cursor()
    query_string = "SELECT * FROM Products WHERE pname LIKE %s"  # Use this select template to fetch data from database
    cur.execute(query_string, (input_string,))  # This one toooo
    searchfetch = cur.fetchall()

    return searchfetch


def update_product_sale(product_id, sale):
    cur = mysql.connection.cursor()
    query_string = " UPDATE Products SET sale = %s WHERE pid = %s"
    cur.execute(query_string, (sale, product_id))
    mysql.connection.commit()
    cur.close()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

if __name__ == '__main__':
    app.run(host="0.0.0.0")













